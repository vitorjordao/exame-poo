using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Kanban.Models;

namespace Kanban.Controllers
{
    public class TarefaController : Controller
    {

        protected readonly TarefaData tarefaData;

        public TarefaController(TarefaData tarefaData)
        {
            this.tarefaData = tarefaData;
        }

        public List<Tarefa> Read()
        {
            return tarefaData.Read();
        }

        public Tarefa Read(int TarefaId)
        {
            return tarefaData.Read(TarefaId);
        }

        public IActionResult Pagina()
        {
            return View(Read());
        }
    }
}

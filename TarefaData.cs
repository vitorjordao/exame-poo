using Kanban.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Kanban
{
    public class TarefaData : DbContext
    {
        public TarefaData(DbContextOptions options) : base(options) 
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Tarefa>().HasKey(t => t.TarefaId);
        }

        public List<Tarefa> Read(){
            return base.Set<Tarefa>().ToList();
        }

        public Tarefa Read(int TarefaId){
            return base.Set<Tarefa>().Find(TarefaId);
        }
    }
}
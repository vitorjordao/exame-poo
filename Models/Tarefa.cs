using System;
namespace Kanban.Models
{
    public class Tarefa
    {
        public int TarefaId{get; set;}
        public string Quadro{get; set;}
        public string Descricao{get; set;} 

        public Tarefa(){}

        public Tarefa(int TarefaId, string Quadro, string Descricao){
            this.TarefaId = TarefaId;
            this.Quadro = Quadro;
            this.Descricao = Descricao;
        }
    }
}